#!/usr/bin/env bash

gulp build
cp -R ./dist/* ../app-common/node_modules/@catenon/salte-auth
# cp -R ./dist/* ../catenon-ui-user/bower_components/catenon-ui-common
# cp -R ./dist/* ../catenon-ui-operation/bower_components/catenon-ui-common
# cp -R ./dist/* ../catenon-ui-candidate/bower_components/catenon-ui-common
# cp -R ./dist/* ../catenon-ui-client/bower_components/catenon-ui-common
# cp -R ./dist/* ../catenon-ui-recruitment-source/bower_components/catenon-ui-common
